<?php
namespace AppBundle\Core;

interface SingletonInterface
{
    /**
     * @return SingletonInterface
     */
    public static function getInstance();
}

