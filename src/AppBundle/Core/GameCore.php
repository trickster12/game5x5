<?php
namespace AppBundle\Core;

class GameCore implements SingletonInterface
{
    use SingletonTrait;

    const HEIGHT = 5;
    const WIDTH = 5;
    const PROBABILITY_INT = 12;
    /** @var  array $cells */
    private $cells;

    /**
     * GameCore constructor.
     */
    public function __construct()
    {
        $this->fillCells();
    }

    /**
     * @param $height
     * @param $width
     *
     * @return array
     */
    public function action($height, $width)
    {
        $this->cells[$height][$width] = 1;
        $pointIm1Jm1 = &$this->cells[$height - 1][$width - 1];
        $pointIm1J = &$this->cells[$height - 1][$width];
        $pointIm1Jp1 = &$this->cells[$height - 1][$width + 1];
        $pointIJp1 = &$this->cells[$height][$width + 1];
        $pointIJm1 = &$this->cells[$height][$width - 1];
        $pointIp1Jm1 = &$this->cells[$height + 1][$width - 1];
        $pointIp1J = &$this->cells[$height + 1][$width];
        $pointIp1Jp1 = &$this->cells[$height + 1][$width + 1];

        if (isset($pointIm1Jm1)) {
            $pointIm1Jm1 = 0 === $pointIm1Jm1 ? 1 : 0;
        }

        if (isset($pointIm1J)) {

            $pointIm1J = 0 === $pointIm1J ? 1 : 0;
        }

        if (isset($pointIm1Jp1)) {
            $pointIm1Jp1 = 0 === $pointIm1Jp1 ? 1 : 0;
        }

        if (isset($pointIJp1)) {
            $pointIJp1 = 0 === $pointIJp1 ? 1 : 0;
        }

        if (isset($pointIJm1)) {
            $pointIJm1 = 0 === $pointIJm1 ? 1 : 0;
        }

        if (isset($pointIp1Jm1)) {
            $pointIp1Jm1 = 0 === $pointIp1Jm1 ? 1 : 0;
        }

        if (isset($pointIp1J)) {
            $pointIp1J = 0 === $pointIp1J ? 1 : 0;
        }

        if (isset($pointIp1Jp1)) {
            $pointIp1Jp1 = 0 === $pointIp1Jp1 ? 1 : 0;
        }

        return $this->cells;
    }

    /**
     *  Fill all Cells
     */
    private function fillCells()
    {
        $this->cells = [];
        for ($height = 0; $height < self::HEIGHT; $height++) {
            for ($width = 0; $width < self::WIDTH; $width++) {
                $this->cells[$height][$width] = 0;
            }
        }
    }

    /**
     * @return bool
     */
    public function checkWin()
    {
        $countCells = self::HEIGHT * self::WIDTH;
        $countTrueCells = 0;
        for ($height = 0; $height < self::HEIGHT; $height++) {
            for ($width = 0; $width < self::WIDTH; $width++) {
                if (1 === $this->cells[$height][$width]) {
                    ++ $countTrueCells;
                }
            }
        }

        return $countCells === $countTrueCells;
    }

    /**
     * Delete rand one cell
     */
    public function randDownOneCell()
    {
        $onCells = [];
        for ($height = 0; $height < self::HEIGHT; $height++) {
            for ($width = 0; $width < self::WIDTH; $width++) {
                if (1 === $this->cells[$height][$width]) {
                    $onCells[] = ['height' => $height, 'width' => $width];
                }
            }
        }
        $indexRandOff = random_int(0, count($onCells));
        $this->cells[$onCells[$indexRandOff]['height']][$onCells[$indexRandOff]['width']] = 0;
    }

    /**
     *
     * @return bool
     */
    public function probability()
    {
        return self::PROBABILITY_INT === random_int(1, 25);
    }

    /**
     * @return array
     */
    public function getCells()
    {
        return $this->cells;
    }

}