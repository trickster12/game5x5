<?php
namespace AppBundle\Core;

trait SingletonTrait
{
    /**
     * @var SingletonInterface
     */
    protected static $instance;

    /**
     * @return SingletonInterface
     */
    final public static function getInstance()
    {
        if (!static::$instance) {
            static::$instance = new static();
        }

        return static::$instance;
    }

}