<?php

namespace AppBundle\Controller;

use AppBundle\Core\GameCore;
use AppBundle\Entity\Result;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;

class DefaultController extends Controller
{
    /** @var GameCore $coreGame */
    private $coreGame;

    public function __construct()
    {
        $this->coreGame = GameCore::getInstance();
    }


    /**
     * @Route("/", name="homepage")
     */
    public function indexAction(Request $request)
    {
        return $this->render('default/index.html.twig', [
            'base_dir' => realpath($this->getParameter('kernel.root_dir').'/..'),
            'cells' => $this->getAllCells($request),
        ]);
    }

    /**
     * @Route("/play", name="play")
     */
    public function playAction(Request $request)
    {
        return new JsonResponse($this->processingClick($request));
    }

    /**
     * @Route("/save", name="save result")
     */
    public function saveAction(Request $request)
    {
        $this->saveResult($request);

        return $this->redirectToRoute('homepage');
    }

    /**
     * @param Request $request
     */
    private function saveResult(Request $request)
    {
        $name = $request->request->get('name');
        $em = $this->getDoctrine()->getManager();
        $result = new Result();
        $result->setName($name);
        $result->setCountClick($request->getSession()->get('countClick'));
        $em->persist($result);
        $em->flush();

        $request->getSession()->set('player', $name);
    }

    /**
     * @param Request $request
     *
     * @return array
     */
    private function processingClick(Request $request)
    {
        $height = $request->request->get('height');
        $width = $request->request->get('width');

        if (!empty($request->getSession()->get('cells'))) {
            $this->coreGame = unserialize($request->getSession()->get('cells'));
        }
        if ($this->coreGame->probability()) {
            $this->coreGame->randDownOneCell();
        }

        $cells = $this->coreGame->action($height, $width);
        $request->getSession()->set('cells', serialize($this->coreGame));
        $countClick = $request->getSession()->get('countClick');
        ++$countClick;

        $request->getSession()->set('countClick', $countClick);

        return [
            'cells' => $cells,
            'countClick' => str_pad($countClick, 3, "0", STR_PAD_LEFT),
            'win' => $this->coreGame->checkWin(),
            'playeWin' => $request->getSession()->get('player') ? $request->getSession()->get('player') : false,
        ];
    }

    /**
     * @return array
     */
    private function getAllCells(Request $request)
    {
        $request->getSession()->set('cells', serialize($this->coreGame));
        $request->getSession()->set('countClick', 0);
        $allCells = [];
        $i = 0;
        foreach ($this->coreGame->getCells() as $height => $cell) {
            foreach ($cell as $width => $item) {
                $allCells[] = ['key' => $i, 'height' => $height, 'width' => $width, 'value' => $item];
                ++$i;
            }
        }

        return $allCells;
    }


}