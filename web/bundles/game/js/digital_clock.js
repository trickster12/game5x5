function initCount(c) {
	var tag = 'div.time';
	var dots = digits = '';
	var digit = tag+' span';
	var span = digit+':nth-child';
	for (i=1; i<6; i++) for (k=1; k<6; k++) dots += '<b class="c'+i+k+'"/>';
	for (i=0; i<8; i++) digits += '<span/>';
	$(tag).append(digits);
	$(digit).append(dots);
	function start(c) {
		var counter = c;
		$(digit+'.colon').css({opacity: 1}).each(function() {
			$(this).delay(400).animate({opacity: 0},250);
		})
		$(digit).removeAttr('class');
		$(span+'(1)').addClass('d'+counter.slice(0,1));
		$(span+'(2)').addClass('d'+counter.slice(1,2));
		$(span+'(3)').addClass('d'+counter.slice(2,3));
	}
	start(c);
}